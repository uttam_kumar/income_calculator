
package com.uttam.income;

/*
// created by uttam kumar
// for handling history listview management
 */

        import android.content.Context;
        import android.database.Cursor;
        import android.graphics.Color;
        import android.text.TextUtils;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.RelativeLayout;
        import android.widget.TextView;


        import java.util.ArrayList;


public class CustomIncomeAdapter extends BaseAdapter {

    private String[] data_type;
    private String[] data_time;
    private String[] data_date;
    private String[] data_amount;
    private String[] data_des;
    Context context;
    private LayoutInflater inflater;

    public CustomIncomeAdapter(Context context, String[] data_time, String[] data_date, String[] data_des, String[] data_amount){
        this.context=context;
        this.data_des=data_des;
        this.data_time=data_time;
        this.data_date=data_date;
        this.data_amount=data_amount;
    }

    @Override
    public int getCount() {
        return data_des.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){

            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.list_item_in_ex, viewGroup,false);

        }

        TextView des_text=view.findViewById(R.id.descriptionId);
        TextView date_text=view.findViewById(R.id.dateId);
        TextView time_text=view.findViewById(R.id.timeId);
        TextView amount_text=view.findViewById(R.id.amountId);

        des_text.setSingleLine(true);
        des_text.setEllipsize(TextUtils.TruncateAt.END);
        des_text.setText(data_des[i]);
        date_text.setText(data_date[i]);
        time_text.setText(data_time[i]);
        amount_text.setText(data_amount[i]);


//
//        //single line & last at 3 dots
//        time_text.setSingleLine(true);
//        time_text.setEllipsize(TextUtils.TruncateAt.END);
//
//        //single line & last at 3 dots
//        url_text.setSingleLine(true);
//        url_text.setEllipsize(TextUtils.TruncateAt.END);


        return view;
    }
}
package com.uttam.income;
/*
// created by uttam kumar
// for handling any view visibility management
*/
import android.view.View;

public class ViewVisibility {

    //used for visible of views
    public void viewVisibilityVisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }

    //used for invisible of views
    public void viewVisibilityInvisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.INVISIBLE);
        }
    }

    //used for visibility gone of views
    public void viewVisibilityGone(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }

}

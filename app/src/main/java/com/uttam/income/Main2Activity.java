package com.uttam.income;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main2Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private static final int PHOTO_GALLERY_PERMISSION=123;
    private TextView balanceTxt;
    private double availableBalance=0;

    //profile
    private ImageView profileImage;
    private Button saveProfileBtn;
    private Uri filePath;
    private Bitmap bitmap;
    private byte[] byteArray;
    private ByteArrayOutputStream stream;
    private TextInputEditText p_name,p_email;
    private TextView errorText;
    private long sizeOfImage;

    //add income
    private Button saveIncomeBtn;
    private TextInputEditText incomeDesc,incomeAmount;


    //add expense
    private Button saveExpenseBtn;
    private TextInputEditText expenseDesc,expenseAmount;

    //add note
    private Button saveNoteBtn;
    private TextInputEditText noteDesc,noteTitle, noteAmount;

    //navigation header
    private ImageView headerImage;
    private TextView headerName,headerEmail;

    //backPressed
    private boolean doubleBackToExitPressedOnce=false;

    //monthly view
    private Spinner dropdown;
    private ArrayAdapter<String> spinnerAdapter;

    private RelativeLayout profileLayout,incomeLayout,expenseLayout,monthViewLayout,homeViewLayout;
    private RelativeLayout incomeListLayout,expenseListLayout,allDataListLayout,noteLayout,noteListLayout;
    private CheckRuntimePermission checkRuntimePermission;
    private ViewVisibility viewVisibility;
    private ProfileDatabase profileDatabase;


    CustomAdapter customAdapter;
    CustomIncomeAdapter customIncomeAdapter;
    CustomNoteAdapter customNoteAdapter;
    ListView listViewIncomeExpense,incomeList,expenseList,noteList;
    ArrayList<String> listDataType=new ArrayList<>();
    ArrayList<String> listDataTime=new ArrayList<>();
    ArrayList<String> listDataDate=new ArrayList<>();
    ArrayList<String> listDataDes=new ArrayList<>();
    ArrayList<String> listDataAmount=new ArrayList<>();

    String[] listData_ty;
    String[] listData_ti;
    String[] listData_da;
    String[] listData_des;
    String[] listData_amo;
    String[] list_type;
    String[] list_time;
    String[] list_date;
    String[] list_des;
    String[] list_amo;


    TextView timeF;
    String tttime="";
    Date dd=null;
    private InputMethodManager imm;

    private SwipeRefreshLayout noteRefresh,incomeRefresh,expenseRefresh,incomeExpenseRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        assignClass();
        findingRelativeLayout();
        assignViews();
        assignListener();

        setupToolbarMenu();
        setupNavigationDrawerMenu();
        availableBalance=profileDatabase.getBalanceData();
        Log.d("TAG", "onCreate: profileDatabase.getBa"+profileDatabase.getBalanceData());
        balanceTxt.setText("Tk. "+String.valueOf(availableBalance));

        String[] items = new String[]{"Select a month","January", "February", "March","April","May","June","july","August"
                ,"September","October","November","December"};
        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(spinnerAdapter);
    }

    private void assignClass(){
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        checkRuntimePermission=new CheckRuntimePermission(Main2Activity.this,Main2Activity.this);
        viewVisibility=new ViewVisibility();
        profileDatabase=new ProfileDatabase(Main2Activity.this);
        checkRuntimePermission.checkPermission();
    }

    private void assignViews() {
        //balance text on toolbar
        balanceTxt=findViewById(R.id.balanceTxtId);
        //profile views
        profileImage=findViewById(R.id.imageFieldId);
        saveProfileBtn=findViewById(R.id.saveProfileBtnId);
        p_name=findViewById(R.id.nameFieldId);
        p_email=findViewById(R.id.emailFieldId);
        errorText=findViewById(R.id.errorTextId);

        //3 dot
        p_name.setSingleLine(true);
        p_email.setSingleLine(true);
        p_name.setEllipsize(TextUtils.TruncateAt.END);
        p_email.setEllipsize(TextUtils.TruncateAt.END);
        //monthly view views
        dropdown = findViewById(R.id.spinner1);

        //income views
        incomeDesc=findViewById(R.id.descriptionIncomeFieldId);
        incomeAmount=findViewById(R.id.amountIncomeFieldId);
        saveIncomeBtn=findViewById(R.id.saveIncomeBtnId);

        //expense views
        expenseDesc=findViewById(R.id.descriptionExpenseFieldId);
        expenseAmount=findViewById(R.id.amountExpenseFieldId);
        saveExpenseBtn=findViewById(R.id.saveExpenseBtnId);

        //add note views
        noteTitle=findViewById(R.id.noteTitleFieldId);
        noteAmount=findViewById(R.id.noteAmountFieldId);
        noteDesc=findViewById(R.id.noteDesFieldId);
        saveNoteBtn=findViewById(R.id.saveNoteBtnId);

        listViewIncomeExpense=findViewById(R.id.listViewIncomeExpenseId);
        incomeList=findViewById(R.id.incomeListId);
        expenseList=findViewById(R.id.expenseListId);
        noteList=findViewById(R.id.noteListId);


        noteRefresh=findViewById(R.id.noteListSwipeRefreshId);
        incomeRefresh=findViewById(R.id.incomeListSwipeRefreshId);
        expenseRefresh=findViewById(R.id.expenseListSwipeRefreshId);
        incomeExpenseRefresh=findViewById(R.id.incomeExpenseListSwipeRefreshId);


//        timeF=findViewById(R.id.timeFID);
    }

    private void assignListener() {
        profileImage.setOnClickListener(this);
        saveProfileBtn.setOnClickListener(this);
        saveIncomeBtn.setOnClickListener(this);
        saveExpenseBtn.setOnClickListener(this);
        saveNoteBtn.setOnClickListener(this);

        noteRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                noteListData();
            }
        });

        incomeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                incomeData();
            }
        });
        expenseRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                expenseData();
            }
        });
        incomeExpenseRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                totalIncomeExpenseData();
            }
        });
    }

    private void findingRelativeLayout() {
        profileLayout=findViewById(R.id.profileLayoutId);
        incomeLayout=findViewById(R.id.addIncomeLayoutId);
        expenseLayout=findViewById(R.id.addExpenseLayoutId);
        monthViewLayout=findViewById(R.id.monthlyViewLayoutId);
        homeViewLayout=findViewById(R.id.homeLayoutId);
        incomeListLayout=findViewById(R.id.incomeListLayoutId);
        expenseListLayout=findViewById(R.id.expenseListLayoutId);
        allDataListLayout=findViewById(R.id.incomeExpenseListLayoutId);
        noteLayout=findViewById(R.id.noteLayoutId);
        noteListLayout=findViewById(R.id.noteListLayoutId);
    }


//    public void storeTime(View v){
//        String date1 =new SimpleDateFormat("hh:mm a, dd/MM/yyyy").format(new Date());
//        timeF.setText(date1);
//        Log.d("TAG", "displayTime: "+date1);
//    }
//
//    public void displayTime(View v){
//        String sd=timeF.getText().toString();
//        try {
//            dd= new SimpleDateFormat("hh:mm a, dd/MM/yyyy").parse(sd);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        Log.d("TAG", "displayTime: "+dd.toString());
//        //Toast.makeText(this,"Time: "+tttime,Toast.LENGTH_SHORT).show();
//    }

    private String currentTime(){
        String time1  =new SimpleDateFormat("dd MMM yy").format(new Date());
        return time1;
    }

    private String currentDate(){
        Date d=new Date();
        Log.d("TAG", "currentDate: "+d);
        String date1 =new SimpleDateFormat("hh:mm a").format(new Date());
        return date1;
    }

    private void setupToolbarMenu() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Home");
    }

    private void setupNavigationDrawerMenu() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        View headerView = navigationView.getHeaderView(0);
        headerName=headerView.findViewById(R.id.txvNameHeaderId);
        headerEmail=headerView.findViewById(R.id.txvEmailHeaderId);
        headerImage=headerView.findViewById(R.id.txvImageHeaderId);
        assignViews();
        displayFirst();
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, R.string.drawer_open, R.string.drawer_close);

        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override // Called when Any Navigation Item is Clicked
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        String itemName = (String) menuItem.getTitle();
        closeDrawer();
        switch (menuItem.getItemId()) {
            case R.id.item_homeId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(homeViewLayout);
                currentDate();
                break;

            case R.id.item_addIncomeId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,homeViewLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible( incomeLayout);
                break;

            case R.id.item_addExpenseId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,homeViewLayout,incomeLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible( expenseLayout);
                break;

            case R.id.item_profileId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,expenseLayout,homeViewLayout,incomeLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible( profileLayout);
                displayFirst();
                break;

            case R.id.item_monthlViewId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,homeViewLayout,incomeLayout,expenseLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible( monthViewLayout);
                break;

            case R.id.item_incomeListId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,homeViewLayout,incomeLayout,expenseLayout,monthViewLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(incomeListLayout );
                incomeData();
                break;

            case R.id.item_expenseListId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,homeViewLayout,incomeLayout,expenseLayout,incomeListLayout,monthViewLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible( expenseListLayout);
                expenseData();
                break;

            case R.id.item_totalSummaryId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,homeViewLayout,incomeLayout,expenseLayout,incomeListLayout,expenseListLayout,monthViewLayout);
                viewVisibility.viewVisibilityVisible( allDataListLayout);
                totalIncomeExpenseData();
                break;

            case R.id.item_languageId:
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(homeViewLayout);
                languageAlert();
                break;

            case R.id.item_aboutId:
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(homeViewLayout);
                aboutAlert();
                break;

            case R.id.item_privecyPolicyId:
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(homeViewLayout);
                privacyAlert();
                break;

            case R.id.item_addNoteId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteListLayout,homeViewLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(noteLayout);
                break;

            case R.id.item_noteListId:
                mToolbar.setTitle(itemName);
                viewVisibility.viewVisibilityGone(noteLayout,homeViewLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(noteListLayout);
                noteListData();
                break;

            case R.id.item_remove_dataId:
                deleteAllAlert();
                break;
        }
        return true;
    }

    private void privacyAlert() {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Main2Activity.this);
        alertDialog2.setCancelable(false);
        alertDialog2.setTitle("Privacy policy");
        alertDialog2.setMessage("This part is on under working");

        alertDialog2.setPositiveButton("Exit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mToolbar.setTitle("Home");
                        dialog.cancel();
                    }
                });
        alertDialog2.show();
    }

    private void noteListData() {
        removeArrayItem();
        if(profileDatabase.numberOfNoteRows()>0){
            //here listDataType is the title
            Cursor cursor1=profileDatabase.getNoteData();
            listDataType.clear();
            listDataTime.clear();
            listDataDate.clear();
            listDataDes.clear();
            listDataAmount.clear();

            if(cursor1.getCount()==0){
                Log.d("TAG", "showHistory: "+ "No available data");
                //return;
            }else {
                while (cursor1.moveToNext()) {
                    listDataType.add(cursor1.getString(1));
                    listDataDes.add(cursor1.getString(2));
                    listDataAmount.add(cursor1.getString(3));
                    listDataTime.add(cursor1.getString(4));
                    listDataDate.add(cursor1.getString(5));
                }


                for(int y=0;y<listDataType.size();y++){
                    listData_ty=listDataType.toArray(new String[0]);
                    listData_ti=listDataTime.toArray(new String[0]);
                    listData_da=listDataDate.toArray(new String[0]);
                    listData_des=listDataDes.toArray(new String[0]);
                    listData_amo=listDataAmount.toArray(new String[0]);
                }

                int length_url=listData_ty.length-1;
                list_type=new String[length_url+1];
                list_time=new String[length_url+1];
                list_date=new String[length_url+1];
                list_des=new String[length_url+1];
                list_amo=new String[length_url+1];

                for(int o=0;o<listData_ty.length;o++){
                    list_type[o]=listData_ty[length_url];
                    list_time[o]=listData_ti[length_url];
                    list_date[o]=listData_da[length_url];
                    list_des[o]=listData_des[length_url];
                    list_amo[o]=listData_amo[length_url];
                    length_url--;
                }
            }

            customNoteAdapter=new CustomNoteAdapter(Main2Activity.this,list_type,list_time,list_date,list_amo);
            noteList.setAdapter(customNoteAdapter);
            customNoteAdapter.notifyDataSetChanged();

            noteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    String dess= list_des[i];
                    Toast.makeText(Main2Activity.this,dess,Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            customNoteAdapter=new CustomNoteAdapter(Main2Activity.this,list_type,list_time,list_date,list_amo);
            noteList.setAdapter(customNoteAdapter);
            customNoteAdapter.notifyDataSetChanged();
            Toast.makeText(this,"No available data",Toast.LENGTH_SHORT).show();
        }
        noteRefresh.setRefreshing(false);
    }

    RadioButton radio_1,radio_2;
    String radioSelected="English";
    private void languageAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.language_alert,null);
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        final TextView setBtn =  view.findViewById(R.id.setId);

        radio_1 = (RadioButton) view.findViewById(R.id.radio1Id);
        radio_2 = (RadioButton) view.findViewById(R.id.radio2Id);
        //radio_1.setClickable(false);
        //if ok button is clicked, close the custom dialog
        setBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radio_1.isChecked()) {
                    radioSelected = radio_1.getText().toString();
                } else if (radio_2.isChecked()) {
                    radioSelected = radio_2.getText().toString();
                }

                Toast.makeText(Main2Activity.this,radioSelected,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void deleteAllAlert() {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Main2Activity.this);
        alertDialog2.setTitle("Delete All");
        alertDialog2.setMessage("It remove your all history data. Are you sure to remove?");

        alertDialog2.setPositiveButton("Remove",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                        viewVisibility.viewVisibilityVisible(homeViewLayout);
                        removeArrayItem();
                        mToolbar.setTitle("Home");
                        balanceTxt.setText("Tk. 0.0");
                        profileDatabase.deleteAllData();
                        dialog.cancel();
                    }
                });

        alertDialog2.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog2.show();
    }

    private void removeArrayItem() {
        listDataType.clear();
        listDataTime.clear();
        listDataDate.clear();
        listDataDes.clear();
        listDataAmount.clear();

        listData_ty=new String[0];
        listData_ti=new String[0];
        listData_da=new String[0];
        listData_des=new String[0];
        listData_amo=new String[0];
        list_type=new String[0];
        list_time=new String[0];
        list_date=new String[0];
        list_des=new String[0];
        list_amo=new String[0];
    }

    private void aboutAlert() {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Main2Activity.this);
        alertDialog2.setCancelable(false);
        alertDialog2.setTitle("About developer");
        alertDialog2.setMessage("Developer: Uttam Kumar Biswas\nEmail: uttamkumarcseiu@gmail.com\nWork place: Synchronous ICT\nDesignation: Asst. Software Engineer(Android & iOS)");

        alertDialog2.setPositiveButton("Exit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mToolbar.setTitle("Home");
                        dialog.cancel();
                    }
                });
        alertDialog2.show();
    }

    private void totalIncomeExpenseData() {
        removeArrayItem();
        if(profileDatabase.numberOfIncomeExpenseRows()>0){
            Cursor cursor1=profileDatabase.getIncomeExpenseData();
            listDataType.clear();
            listDataTime.clear();
            listDataDate.clear();
            listDataDes.clear();
            listDataAmount.clear();

            if(cursor1.getCount()==0){
                Log.d("TAG", "showHistory: "+ "No available data");
                //return;
            }else {
                while (cursor1.moveToNext()) {
                    listDataType.add(cursor1.getString(1));
                    listDataTime.add(cursor1.getString(2));
                    listDataDate.add(cursor1.getString(3));
                    listDataDes.add(cursor1.getString(4));
                    listDataAmount.add(cursor1.getString(5));
                }

                for(int y=0;y<listDataType.size();y++){
                    listData_ty=listDataType.toArray(new String[0]);
                    listData_ti=listDataTime.toArray(new String[0]);
                    listData_da=listDataDate.toArray(new String[0]);
                    listData_des=listDataDes.toArray(new String[0]);
                    listData_amo=listDataAmount.toArray(new String[0]);
                }

                int length_url=listData_ty.length-1;
                list_type=new String[length_url+1];
                list_time=new String[length_url+1];
                list_date=new String[length_url+1];
                list_des=new String[length_url+1];
                list_amo=new String[length_url+1];

                for(int o=0;o<listData_ty.length;o++){
                    list_type[o]=listData_ty[length_url];
                    list_time[o]=listData_ti[length_url];
                    list_date[o]=listData_da[length_url];
                    list_des[o]=listData_des[length_url];
                    list_amo[o]=listData_amo[length_url];
                    length_url--;
                }
            }

            customAdapter=new CustomAdapter(Main2Activity.this,list_type,list_time,list_date,list_amo);
            listViewIncomeExpense.setAdapter(customAdapter);
            customAdapter.notifyDataSetChanged();

            listViewIncomeExpense.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    String type= list_type[i];
                    Toast.makeText(Main2Activity.this,type,Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            customAdapter=new CustomAdapter(Main2Activity.this,list_type,list_time,list_date,list_amo);
            listViewIncomeExpense.setAdapter(customAdapter);
            customAdapter.notifyDataSetChanged();
            Toast.makeText(this,"No available data",Toast.LENGTH_SHORT).show();
        }
        incomeExpenseRefresh.setRefreshing(false);
    }

    private void incomeData() {
        removeArrayItem();
        if(profileDatabase.numberOfIncomeRows()>0){
            Cursor cursor1=profileDatabase.getIncomeExpenseData();
            listDataType.clear();
            listDataTime.clear();
            listDataDate.clear();
            listDataDes.clear();
            listDataAmount.clear();

            if(cursor1.getCount()==0){
                Log.d("TAG", "showHistory: "+ "No available data");
                //return;
            }else {
                while (cursor1.moveToNext()) {
                    String typpt=cursor1.getString(1);
                    if(typpt.equals("Income")) {
                        listDataTime.add(cursor1.getString(2));
                        listDataDate.add(cursor1.getString(3));
                        listDataDes.add(cursor1.getString(4));
                        listDataAmount.add(cursor1.getString(5));
                    }
                }


                for(int y=0;y<listDataDes.size();y++){
                    listData_ti=listDataTime.toArray(new String[0]);
                    listData_da=listDataDate.toArray(new String[0]);
                    listData_des=listDataDes.toArray(new String[0]);
                    listData_amo=listDataAmount.toArray(new String[0]);
                }

                int length_url=listData_des.length-1;
                list_time=new String[length_url+1];
                list_date=new String[length_url+1];
                list_des=new String[length_url+1];
                list_amo=new String[length_url+1];

                for(int o=0;o<listData_des.length;o++){
                    list_time[o]=listData_ti[length_url];
                    list_date[o]=listData_da[length_url];
                    list_des[o]=listData_des[length_url];
                    list_amo[o]=listData_amo[length_url];
                    length_url--;
                }
            }

            customIncomeAdapter=new CustomIncomeAdapter(Main2Activity.this,list_time,list_date,list_des,list_amo);
            incomeList.setAdapter(customIncomeAdapter);
            customIncomeAdapter.notifyDataSetChanged();

            incomeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    String desd= list_des[i];
                    Toast.makeText(Main2Activity.this,desd,Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            customIncomeAdapter=new CustomIncomeAdapter(Main2Activity.this,list_time,list_date,list_des,list_amo);
            incomeList.setAdapter(customIncomeAdapter);
            customIncomeAdapter.notifyDataSetChanged();
            Toast.makeText(this,"No available data",Toast.LENGTH_SHORT).show();
        }
        incomeRefresh.setRefreshing(false);
    }

    private void expenseData() {
        removeArrayItem();
        if(profileDatabase.numberOfExpenseRows()>0){
            Cursor cursor1=profileDatabase.getIncomeExpenseData();
            listDataType.clear();
            listDataTime.clear();
            listDataDate.clear();
            listDataDes.clear();
            listDataAmount.clear();

            if(cursor1.getCount()==0){
                Log.d("TAG", "showHistory: "+ "No available data");
                //return;
            }else {
                while (cursor1.moveToNext()) {
                    String typpt=cursor1.getString(1);
                    if(typpt.equals("Expense")) {
                        listDataTime.add(cursor1.getString(2));
                        listDataDate.add(cursor1.getString(3));
                        listDataDes.add(cursor1.getString(4));
                        listDataAmount.add(cursor1.getString(5));
                    }
                }


                for(int y=0;y<listDataDes.size();y++){
                    listData_ti=listDataTime.toArray(new String[0]);
                    listData_da=listDataDate.toArray(new String[0]);
                    listData_des=listDataDes.toArray(new String[0]);
                    listData_amo=listDataAmount.toArray(new String[0]);
                }

                int length_url=listData_des.length-1;
                list_time=new String[length_url+1];
                list_date=new String[length_url+1];
                list_des=new String[length_url+1];
                list_amo=new String[length_url+1];

                for(int o=0;o<listData_des.length;o++){
                    list_time[o]=listData_ti[length_url];
                    list_date[o]=listData_da[length_url];
                    list_des[o]=listData_des[length_url];
                    list_amo[o]=listData_amo[length_url];
                    length_url--;
                }
            }

            customIncomeAdapter=new CustomIncomeAdapter(Main2Activity.this,list_time,list_date,list_des,list_amo);
            expenseList.setAdapter(customIncomeAdapter);
            customIncomeAdapter.notifyDataSetChanged();

            expenseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    String desd= list_des[i];
                    Toast.makeText(Main2Activity.this,desd,Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            customIncomeAdapter=new CustomIncomeAdapter(Main2Activity.this,list_time,list_date,list_des,list_amo);
            expenseList.setAdapter(customIncomeAdapter);
            customIncomeAdapter.notifyDataSetChanged();
            Toast.makeText(this,"No available data",Toast.LENGTH_SHORT).show();
        }
        expenseRefresh.setRefreshing(false);
    }

    // Close the Drawer
    private void closeDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    // Open the Drawer
    private void showDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        }else {
            if(mToolbar.getTitle().toString().equals("Home")) {
                if (doubleBackToExitPressedOnce) {
                    finishAffinity();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
            }else{
                viewVisibility.viewVisibilityGone(noteListLayout,noteLayout,profileLayout,incomeLayout,expenseLayout,monthViewLayout,incomeListLayout,expenseListLayout,allDataListLayout);
                viewVisibility.viewVisibilityVisible(homeViewLayout);
                mToolbar.setTitle("Home");
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imageFieldId:
                checkRuntimePermission.checkPermission();
                if(checkRuntimePermission.isPermissionNotGranted()){
                    Intent intent=new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent,"Select Image"),PHOTO_GALLERY_PERMISSION);
                }else{
                    Log.d("TAG", "onClick: luiuito");
                }
                break;

            case R.id.saveProfileBtnId:
                String pName=p_name.getText().toString();
                String pEmail=p_email.getText().toString();
               // String pPhone=p_name.getText().toString();
                //if(byteArray!=null&&pEmail!=null&&pName!=null){
                if(byteArray==null || pEmail.equals("") || pName.equals("") ){
                    //sqLiteDatabaseHelper.insertProfileInfo(pName,pEmail,byteArray);
                    Toast.makeText(this,"Fill up all the field",Toast.LENGTH_SHORT).show();
                } else{
                    Log.d("TAG", "onClick: grehyrty7yu");
                    if(sizeOfImage/1024 > 250){
                        errorText.setTextColor(Color.RED);
                        Log.d("TAG", "onClick: grehyrty7yull");
                        Toast.makeText(this, "Image size more than 250KB", Toast.LENGTH_LONG);
                    }else {
                        errorText.setTextColor(Color.BLACK);
                        Log.d("TAG", "onClick: grehyrty7yullhgh");
                        long STATUS = profileDatabase.insertProfileData(pName, pEmail, byteArray);
                        if (STATUS != -1) {
                            Toast.makeText(this, "Data Inserted", Toast.LENGTH_SHORT).show();
                            headerName.setText(pName);
                            headerEmail.setText(pEmail);
                            headerImage.setImageBitmap(byteArrayToBitmap(byteArray));
                            Log.d("TAG", "onClick: grehyrty7yullgeyyr");
                        } else {
                            Log.d("TAG", "onClick: grehyrty7yullfdhrtru");
                            Toast.makeText(this, "Error On Data Inserted", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                imm.hideSoftInputFromWindow(mDrawerLayout.getWindowToken(), 0);
                break;

            case R.id.saveIncomeBtnId:
                String descI=incomeDesc.getText().toString();
                String amountI=incomeAmount.getText().toString();
                String timI=currentTime();
                String datI=currentDate();

                if(descI.equals("")||amountI.equals("")||timI.equals("")||datI.equals("")){
                    Toast.makeText(this,"Fill up all the field",Toast.LENGTH_SHORT).show();
                }else{
                    long STATU = profileDatabase.insertIncomeData("Income", timI, datI,descI,amountI);
                    availableBalance=availableBalance+Double.valueOf(amountI);
                    long STAT = profileDatabase.insertBalanceData( String.valueOf(availableBalance));
                    balanceTxt.setText("Tk. "+String.valueOf(availableBalance));
                    if (STATU != -1 && STAT!=-1) {
                        incomeDesc.setText("");
                        incomeAmount.setText("");
                        Toast.makeText(this, "Data Inserted", Toast.LENGTH_SHORT).show();
                        //Log.d("TAG", "onClick: grehyrty7yullgeyyr");
                    } else {
                        Log.d("TAG", "onClick: grehyrty7yullfdhrtru");
                        Toast.makeText(this, "Error On Data Inserted", Toast.LENGTH_SHORT).show();
                    }
                }
                imm.hideSoftInputFromWindow(mDrawerLayout.getWindowToken(), 0);
                break;

            case R.id.saveExpenseBtnId:
                String descE=expenseDesc.getText().toString();
                String amountE=expenseAmount.getText().toString();
                String timE=currentTime();
                String datE=currentDate();
                //Toast.makeText(this, "Data Inserted", Toast.LENGTH_SHORT).show();
                if(descE.equals("")||amountE.equals("")||timE.equals("")||datE.equals("")){
                    Toast.makeText(this,"Fill up all the field",Toast.LENGTH_SHORT).show();
                }else{
                    long STAT = profileDatabase.insertIncomeData("Expense", timE, datE,descE,amountE);
                    availableBalance=availableBalance-Double.valueOf(amountE);
                    long STA = profileDatabase.insertBalanceData( String.valueOf(availableBalance));
                    balanceTxt.setText("Tk. "+String.valueOf(availableBalance));
                    if (STAT != -1 && STA!=-1) {
                        expenseDesc.setText("");
                        expenseAmount.setText("");
                        Toast.makeText(this, "Data Inserted", Toast.LENGTH_SHORT).show();
                        //Log.d("TAG", "onClick: grehyrty7yullgeyyr");
                    } else {
                        Log.d("TAG", "onClick: grehyrty7yullfdhrtru");
                        Toast.makeText(this, "Error On Data Inserted", Toast.LENGTH_SHORT).show();
                    }
                }

                imm.hideSoftInputFromWindow(mDrawerLayout.getWindowToken(), 0);
                break;

            case R.id.saveNoteBtnId:
                String titleN=noteTitle.getText().toString();
                String amountN=noteAmount.getText().toString();
                String descN=noteDesc.getText().toString();
                String timN=currentTime();
                String datN=currentDate();
                //Toast.makeText(this, "Data Inserted", Toast.LENGTH_SHORT).show();
                if(titleN.equals("")||descN.equals("")||amountN.equals("")||timN.equals("")||datN.equals("")){
                    Toast.makeText(this,"Fill up all the field",Toast.LENGTH_SHORT).show();
                }else{
                    long nSTAT = profileDatabase.insertNoteData(titleN, amountN, descN, timN, datN);
                    if (nSTAT != -1) {
                        noteTitle.setText("");
                        noteAmount.setText("");
                        noteDesc.setText("");
                        Toast.makeText(this, "Data Inserted", Toast.LENGTH_SHORT).show();
                        //Log.d("TAG", "onClick: grehyrty7yullgeyyr");
                    } else {
                        Log.d("TAG", "onClick: grehyrty7yullfdhrtru");
                        Toast.makeText(this, "Error On Data Inserted", Toast.LENGTH_SHORT).show();
                    }
                }
                imm.hideSoftInputFromWindow(mDrawerLayout.getWindowToken(), 0);
                break;
        }
    }

    private Bitmap byteArrayToBitmap(byte[] byteAr) {
        ByteArrayInputStream inputStream;
        Bitmap bm=null;
        try {
            if (byteAr != null) {
                inputStream = new ByteArrayInputStream(byteAr);
                bm = BitmapFactory.decodeStream(inputStream);
            }
        }catch (Exception e){
            e.getMessage();
        }

        return bm;
    }

    private void displayFirst(){
        profileDatabase=new ProfileDatabase(Main2Activity.this);
        if(profileDatabase.numberOfProfileRows()>0) {
            Cursor rs=profileDatabase.getProfileData();
            rs.moveToFirst();
            String name = rs.getString(rs.getColumnIndex(ProfileDatabase.NAME));
            String email = rs.getString(rs.getColumnIndex(ProfileDatabase.EMAIL));
            byte[] byteArr= rs.getBlob(rs.getColumnIndex(ProfileDatabase.IMAGE));
            byteArray=byteArr;
            if (!rs.isClosed()) {
                rs.close();
            }

            p_name.setText(name);
            p_email.setText(email);
            headerName.setText(name);
            headerEmail.setText(email);
            headerImage.setImageBitmap(byteArrayToBitmap(byteArr));
            profileImage.setImageBitmap(byteArrayToBitmap(byteArr));
            Log.d("TAG", "profile database: name: " + name);
            Log.d("TAG", "profile database: email: " + email);
        }else{
            p_name.setText("");
            p_email.setText("");
            headerName.setText("Hunney doll");
            headerEmail.setText("hunneydoll@gmail.com");
            profileImage.setImageResource(R.drawable.p_images);
            headerImage.setImageResource(R.drawable.p_images);
            //Toast.makeText(this,"No Available Data",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==PHOTO_GALLERY_PERMISSION&&resultCode==RESULT_OK && data!=null){
            filePath=data.getData();
            try{
                InputStream inputStream=getContentResolver().openInputStream(filePath);
                bitmap=BitmapFactory.decodeStream(inputStream);
                profileImage.setImageBitmap(bitmap);

                try {
                    stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byteArray = stream.toByteArray();
                    sizeOfImage = byteArray.length;
                    Log.d("TAG", "onActivityResult: sizeOfImage: "+sizeOfImage);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }catch (Exception e){
                Toast.makeText(this,"file not found",Toast.LENGTH_SHORT).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
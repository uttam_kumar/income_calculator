package com.uttam.income;
/*
// created by uttam kumar
// for handling run time permission management
*/
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;

public class CheckRuntimePermission extends Activity {

    Context mContext;
    Activity mActivity;
    //user permission
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;

    public CheckRuntimePermission(Context mContext, Activity mActivity){
        this.mContext=mContext;
        this.mActivity=mActivity;
    }

    public void checkPermission(){
        if(ContextCompat.checkSelfPermission(mContext,Manifest.permission.CAMERA)
                + ContextCompat.checkSelfPermission(mContext,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(mContext,Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

            // Do something, when permissions not granted
            if(ActivityCompat.shouldShowRequestPermissionRationale(mActivity,Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(mActivity,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(mActivity,Manifest.permission.READ_EXTERNAL_STORAGE)){

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setCancelable(false);
                //builder.setIcon(R.drawable.icon_warning);
                builder.setMessage(Html.fromHtml("Storage permissions are highly required. Otherwise,"+" <b>bla bla bla</b>"+" may crush."));
                //builder.setMessage("Camera, Microphone & Storage permissions are highly required otherwise Synopi Live 360 may crush.");
                builder.setTitle("Permissions required");
                builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                        intent.setData(uri);
                        mActivity.startActivityForResult(intent, MY_PERMISSIONS_REQUEST_CODE);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }else{
                ActivityCompat.requestPermissions(
                        mActivity,
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        }else {
            // Do something, when permissions are already granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CODE:{
                if((grantResults.length >0) && (grantResults[0] + grantResults[1]
                        + grantResults[2] == PackageManager.PERMISSION_GRANTED)){
                    //permission granted
                }else {
                    //permission denied
                    checkPermission();
                }
            }
            break;
        }
    }

    //checking current runtime permission
    public boolean isPermissionNotGranted(){
        boolean granted=false;
        if(ContextCompat.checkSelfPermission(mContext,Manifest.permission.CAMERA)
                + ContextCompat.checkSelfPermission(mContext,Manifest.permission.RECORD_AUDIO)
                + ContextCompat.checkSelfPermission(mContext,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            granted=true;
        }
        return granted;
    }
}

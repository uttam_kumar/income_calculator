package com.uttam.income;

/*
// created by uttam kumar
// for handling history listview management
 */

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;


public class CustomAdapter extends BaseAdapter {

    private String[] data_type;
    private String[] data_time;
    private String[] data_date;
    private String[] data_amount;
    Context context;
    private LayoutInflater inflater;

    public CustomAdapter(Context context, String[] data_type, String[] data_time, String[] data_date, String[] data_amount){
        this.context=context;
        this.data_type=data_type;
        this.data_time=data_time;
        this.data_date=data_date;
        this.data_amount=data_amount;
    }

    @Override
    public int getCount() {
        return data_type.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){

            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.list_item_all, viewGroup,false);

        }

        TextView type_text=view.findViewById(R.id.typeId);
        TextView date_text=view.findViewById(R.id.dateId);
        TextView time_text=view.findViewById(R.id.timeId);
        TextView amount_text=view.findViewById(R.id.amountId);
        if(data_type[i].equals("Income")){
            type_text.setText("I");
            type_text.setBackgroundResource(R.drawable.circular_view_income);
        }else {
            type_text.setBackgroundResource(R.drawable.circular_view_expense);
            type_text.setText("E");
        }

        date_text.setText(data_date[i]);
        time_text.setText(data_time[i]);
        amount_text.setText(data_amount[i]);


//
//        //single line & last at 3 dots
//        time_text.setSingleLine(true);
//        time_text.setEllipsize(TextUtils.TruncateAt.END);
//
//        //single line & last at 3 dots
//        url_text.setSingleLine(true);
//        url_text.setEllipsize(TextUtils.TruncateAt.END);


        return view;
    }

//    private void addAllHistoryData() {
//
//        Cursor cursor = mySqLiteDatabaseHelper.displayAllData();
//
//        //setting list view parent relative layout height
//        ViewGroup.LayoutParams params = list_view_layout.getLayoutParams();
//        int row_number = mySqLiteDatabaseHelper.rowCountFromTable();
//        final float scale = context.getResources().getDisplayMetrics().density;
//        Log.d("TAG", "urlAlert: row_number: " + row_number);
//        if (row_number > 4) {
//            params.height = ((int) (scale * 31 + 0.5f)) * 4;
//        } else {
//            params.height = ((int) (scale * 31 + 0.5f)) * row_number;
//        }
//
//        ArrayList<String> listData_url=new ArrayList<>();
//        ArrayList<String> listData_key=new ArrayList<>();
//        ArrayList<String> listData_time=new ArrayList<>();
//        String[] listData_k=new String[mySqLiteDatabaseHelper.rowCountFromTable()];
//        String[] listData_t=new String[mySqLiteDatabaseHelper.rowCountFromTable()];
//        String[] listData_u=new String[mySqLiteDatabaseHelper.rowCountFromTable()];
//        String[] list_url;
//        String[] list_key;
//        String[] list_time;
//
//        while (cursor.moveToNext()) {
//            listData_url.add(cursor.getString(1));
//            listData_key.add(cursor.getString(2));
//            listData_time.add(cursor.getString(3));
//        }
//
//
//        for (int y = 0; y < listData_url.size(); y++) {
//            listData_u = listData_url.toArray(new String[0]);
//            listData_k = listData_key.toArray(new String[0]);
//            listData_t = listData_time.toArray(new String[0]);
//        }
//
//        int length_url = listData_u.length - 1;
//        list_url = new String[length_url + 1];
//        list_key = new String[length_url + 1];
//        list_time = new String[length_url + 1];
//
//        for (int o = 0; o < listData_u.length; o++) {
//            list_url[o] = listData_u[length_url];
//            list_key[o] = listData_k[length_url];
//            list_time[o] = listData_t[length_url];
//            length_url--;
//        }
//
//        list.setAdapter(new CustomAdapter(context, list_url, list_key, list_time, list,list_view_layout,historyBtn,historyImgBtn,vi_layout,urlSetText));
//
//    }
}
package com.uttam.income;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class ProfileDatabase extends SQLiteOpenHelper {

    public static final int VERSION_CODE = 17;

    String TAG="TAG";

    public static final String DATABASE_NAME = "MyDBName.db";

    //profile information
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String IMAGE = "image";

    //income/expense information
    public static final String TYPE = "type";
    public static final String DESC = "descrip";
    public static final String AMOUNT = "amount";
    public static final String TIME = "time";
    public static final String DATE = "date";

    //adding note
    public static final String NOTE_TITLE = "title";
    public static final String NOTE_AMOUNT = "amount";
    public static final String NOTE_DES = "description";
    public static final String NOTE_TIME = "time";
    public static final String NOTE_DATE = "date";


    public ProfileDatabase(Context mContext) {
        super(mContext, DATABASE_NAME , null, VERSION_CODE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table profile (id integer primary key AUTOINCREMENT, name varchar(255), email varchar(255), image blob)");
        db.execSQL("create table income (id integer primary key AUTOINCREMENT, type varchar(255), time varchar(55), date varchar(55), descrip varchar(255), amount varchar(55))");
        db.execSQL("create table note (id integer primary key AUTOINCREMENT, title varchar(255), description varchar(1255), amount varchar(55), time varchar(55), date varchar(55))");
        db.execSQL("create table totalbalance (id integer primary key AUTOINCREMENT,  balance varchar(255))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS profile");
        db.execSQL("DROP TABLE IF EXISTS income");
        db.execSQL("DROP TABLE IF EXISTS note");
        db.execSQL("DROP TABLE IF EXISTS totalbalance");
        onCreate(db);
    }

    //Balance data part
    public long insertBalanceData (String blnc) {
        SQLiteDatabase db = this.getWritableDatabase();
        long st=-1;
        ContentValues cvs = new ContentValues();
        cvs.put("balance", blnc);
        if(numberOfBalanceRows()>0){
            st=db.update("totalbalance", cvs, "id = 1 ", null );
            Log.d(TAG, "totalbalance tb: updated");
        }else{
            st=db.insert("totalbalance", null, cvs);
            Log.d(TAG, "totalbalance tb: inserted");
        }
        return st;
    }
    public double getBalanceData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from totalbalance where id=1", null );
        String blnc="0.0";
        if(res.getCount()==0){
            Log.d("TAG", "showHistory: "+ "No available data");
            //return;
        }else {
            while (res.moveToNext()) {
                blnc = res.getString(1);
                Log.d(TAG, "getBalanceData: " + res.getString(1));
            }
        }
        res.close();
        db.close();
        return Double.valueOf(blnc);
    }
    public int numberOfBalanceRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRo = (int) DatabaseUtils.queryNumEntries(db, "totalbalance");
        return numRo;
    }

    //profile data part
    public long insertProfileData (String name , String email, byte[] image) {
        SQLiteDatabase db = this.getWritableDatabase();
        long STATUS=-1;
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("email", email);
        cv.put("image", image);
        if(numberOfProfileRows()>0){
            STATUS=db.update("profile", cv, "id = 1 ", null );
            Log.d(TAG, "profile database: updated");
        }else{
            STATUS=db.insert("profile", null, cv);
            Log.d(TAG, "profile database: inserted");
        }
        return STATUS;
    }
    public Cursor getProfileData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from profile where id=1", null );
        return res;
    }
    public int numberOfProfileRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "profile");
        return numRows;
    }

    //income/expense part
    public long insertIncomeData (String ty , String ti, String da , String des,String amo) {
        SQLiteDatabase db = this.getWritableDatabase();
        long STATUS=-1;
        ContentValues cv = new ContentValues();
        cv.put("type", ty);
        cv.put("time", ti);
        cv.put("date", da);
        cv.put("descrip", des);
        cv.put("amount", amo);

        STATUS=db.insert("income", null, cv);
        Log.d(TAG, "income database: inserted");

        return STATUS;
    }
    public Cursor getIncomeExpenseData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cs =  db.rawQuery( "select * from income", null );
        return cs;
    }
    public int numberOfIncomeExpenseRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "income");
        return numRows;
    }
    public int numberOfIncomeRows(){
        int counter=0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cs1 =  db.rawQuery( "select * from income", null );
        if(cs1.getCount()==0){
            Log.d("TAG", "showHistory: "+ "No available data");
            //return;
        }else {
            while (cs1.moveToNext()) {
                String typpt = cs1.getString(1);
                if (typpt.equals("Income")) {
                    counter++;
                }
            }
        }
        return counter;
    }

    public int numberOfExpenseRows(){
        int counter=0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cs1 =  db.rawQuery( "select * from income", null );
        if(cs1.getCount()==0){
            Log.d("TAG", "showHistory: "+ "No available data");
            //return;
        }else {
            while (cs1.moveToNext()) {
                String typpt = cs1.getString(1);
                if (typpt.equals("Expense")) {
                    counter++;
                }
            }
        }
        return counter;
    }

    //note part
    public long insertNoteData (String nTitle , String nAmount, String nDes , String nTime, String nDate) {
        SQLiteDatabase db = this.getWritableDatabase();
        long NSTATUS=-1;
        ContentValues cv = new ContentValues();
        cv.put("title", nTitle);
        cv.put("amount", nAmount);
        cv.put("description", nDes);
        cv.put("time", nTime);
        cv.put("date", nDate);

        NSTATUS=db.insert("note", null, cv);
        Log.d(TAG, "income database: inserted");

        return NSTATUS;
    }

    public Cursor getNoteData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor ncs =  db.rawQuery( "select * from note", null );
        return ncs;
    }

    public int numberOfNoteRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int nRows = (int) DatabaseUtils.queryNumEntries(db, "note");
        return nRows;
    }

    public void deleteAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM income");
        db.execSQL("DELETE FROM note");
        //db.execSQL("DELETE FROM totalbalance");
        insertBalanceData("0.0");
        db.close();
    }
}
